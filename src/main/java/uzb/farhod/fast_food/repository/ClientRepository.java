package uzb.farhod.fast_food.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import uzb.farhod.fast_food.entity.Client;

import java.util.Optional;
import java.util.UUID;

public interface ClientRepository extends JpaRepository<Client, UUID> {
    Optional<Client> findByPhoneNumber(String phoneNumber);
}
