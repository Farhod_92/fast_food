package uzb.farhod.fast_food.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.domain.AuditorAware;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import uzb.farhod.fast_food.entity.User;

import java.util.Optional;

@Configuration
@EnableJpaAuditing(auditorAwareRef = "auditorAware")
public class AuditConfig {
        @Bean
        public AuditorAware<Long> getAuditorId(){
            Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
            if(authentication!=null &&
                    authentication.isAuthenticated() &&
                    !authentication.getPrincipal().equals("anonymousUser")) {
                User user = (User) authentication.getPrincipal();
                return ()-> Optional.of(user.getId());
            }
            return Optional::empty;

        }

}
