package uzb.farhod.fast_food.security;

import lombok.AllArgsConstructor;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;
import uzb.farhod.fast_food.entity.Client;
import uzb.farhod.fast_food.service.AuthService;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.Base64;

@Component
@AllArgsConstructor
public class JwtFilter extends OncePerRequestFilter {
    private final JwtProvider jwtProvider;
    private final AuthService authService;

    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain) throws ServletException, IOException {
        String token = request.getHeader("Authorization");

        if(token!=null && token.startsWith("Bearer")){
            token=token.substring(7);
            String usernameFromToken = jwtProvider.getUsernameFromToken(token);
            UserDetails userDetails = authService.loadUserByUsername(usernameFromToken);

            if(usernameFromToken!=null){
                Authentication authentication =new UsernamePasswordAuthenticationToken(userDetails, null, userDetails.getAuthorities());
                SecurityContextHolder.getContext().setAuthentication(authentication);
            }
        }else if(token!=null && token.startsWith("Basic")){
            token=token.substring(6);
            byte[] decode = Base64
                    .getDecoder()
                    .decode(token);
            String base64Decoded=new String(decode, StandardCharsets.UTF_8);
            String[] split = base64Decoded.split(":");
            String cardNumber=split[0];
            String confirmCode=split[1];
            Client client = authService.loadClientByPhoneNumber(cardNumber);
            if( client.getConfirmCode().equals(confirmCode)){
                UsernamePasswordAuthenticationToken authentication=
                        new UsernamePasswordAuthenticationToken(client,null, client.getAuthorities());
                SecurityContextHolder.getContext().setAuthentication(authentication);
            }
        }
        filterChain.doFilter(request,response);
    }
}
