package uzb.farhod.fast_food.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import uzb.farhod.fast_food.entity.enums.AppType;
import uzb.farhod.fast_food.entity.enums.Language;
import uzb.farhod.fast_food.entity.template.AbsUUIDEntity;

import javax.persistence.*;
import java.util.Collection;

@EqualsAndHashCode(callSuper = true)
@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
public class Client extends AbsUUIDEntity implements UserDetails {
    @Column(nullable = false)
    @Enumerated(EnumType.STRING)
    private Language language;

    @Column(nullable = false, unique = true)
    private String phoneNumber;

    @Column(nullable = false)
    private String fullName;

    @JsonIgnore
    @ManyToOne(fetch = FetchType.LAZY,optional = false)
    private City city;

    private String confirmCode;

    private boolean offertaAgreement;

    private Integer reliabilityRate;

    @Enumerated(EnumType.STRING)
    @Column(nullable = false)
    private AppType appType;

    @Column(unique = true)
    private String telegramChatId;

    private String telegramLastCommand;

    //USERDETAILS
    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return null;
    }

    @Override
    public String getPassword() {
        return this.confirmCode;
    }

    @Override
    public String getUsername() {
        return this.phoneNumber;
    }

    private boolean enabled;
    private boolean accountNonExpired;
    private boolean accountNonLocked;
    private boolean credentialsNonExpired;
}
