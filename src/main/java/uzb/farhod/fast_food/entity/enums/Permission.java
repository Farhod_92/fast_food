package uzb.farhod.fast_food.entity.enums;

public enum Permission {
    ADD_PRODUCT,
    EDIT_PRODUCT,
    DELETE_PRODUCT,
    RECEIVE_ORDER,
    CREATE_ORDER
}
