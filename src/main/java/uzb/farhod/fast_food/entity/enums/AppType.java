package uzb.farhod.fast_food.entity.enums;

public enum AppType {
    TELEGRAM_BOT,
    MOBILE_APP,
    PHONE_CALL
}
