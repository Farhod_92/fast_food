package uzb.farhod.fast_food.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import uzb.farhod.fast_food.entity.enums.Language;
import uzb.farhod.fast_food.entity.enums.Permission;
import uzb.farhod.fast_food.entity.template.AbsLongEntity;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;

@EqualsAndHashCode(callSuper = true)
@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "usr")
public class User extends AbsLongEntity implements UserDetails {

    private String fullName;

    @Column(nullable = false, unique = true)
    private String username;

    @Column(nullable = false)
    private String password;

    @ManyToOne(optional = false)
    private Role role;

    private String phoneNumber;
    private String phoneNumber2;
    private String confirmCode;
    private Date birthDate;

    @Column(nullable = false)
    @Enumerated(EnumType.STRING)
    private Language language;

    @JsonIgnore
    @ManyToOne(fetch = FetchType.LAZY)
    private Attachment attachment;

    private boolean online;
    private boolean free;


    //USERDETAILS
    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        List<Permission> permissions = role.getPermissions();
        List<GrantedAuthority> grantedAuthorities=new ArrayList<>();
        for (Permission permission : permissions) {
            grantedAuthorities.add(new SimpleGrantedAuthority(permission.name()));
        }
        return grantedAuthorities;
    }

    private boolean enabled;
    private boolean accountNonExpired;
    private boolean accountNonLocked;
    private boolean credentialsNonExpired;
}
