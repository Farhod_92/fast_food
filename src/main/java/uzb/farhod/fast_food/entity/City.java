package uzb.farhod.fast_food.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import uzb.farhod.fast_food.entity.template.AbsLongEntity;

import javax.persistence.*;

@EqualsAndHashCode(callSuper = true)
@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table (uniqueConstraints = { @UniqueConstraint(columnNames = {"region_id", "name_uz"}), @UniqueConstraint(columnNames = {"region_id", "name_ru"}) } )
public class City extends AbsLongEntity {
    @Column(nullable = false)
    private String nameUz;

    @Column(nullable = false)
    private String nameRu;

    @ManyToOne(optional = false)
    private Region region;
}
